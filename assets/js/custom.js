/**
 * Created by Anh on 9/27/2016.
 */
document.title = document.title.replace('Page', 'Trang');
$(function(){
    //$('.slickSlider p').slick({
    //    autoplay: true,
    //    infinite: true,
    //    dots: false,
    //    pauseOnHover: true
    //});

    var olderPostsBtn = $('.older-posts');
    var newerPostsBtn = $('.newer-posts');
    var pageNumber = $('.page-number');

    if (olderPostsBtn.length){
        olderPostsBtn.html('Bài cũ hơn <span aria-hidden="true">→</span>');
    }

    if (newerPostsBtn.length){
        newerPostsBtn.html('<span aria-hidden="true">←</span> Bài mới hơn');
    }

    if (pageNumber.length){
        var text = pageNumber.text().replace('Page', 'Trang').replace('of', '/');
        pageNumber.text(text);
    }
});